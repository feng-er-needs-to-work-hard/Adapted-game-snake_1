import pygame
import sys

class ColorButton:
    def __init__(self, pos, color, callback):
        self.rect = pygame.Rect(pos, (100, 50))
        self.color = color
        self.callback = callback

    def draw(self, screen):
        pygame.draw.rect(screen, self.color, self.rect)

    def handle_event(self, event):
        if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
            if self.rect.collidepoint(event.pos):
                self.callback()

class ColorController:
    def __init__(self):
        self.color = (255, 255, 255)  # 初始颜色为白色
        self.buttons = [
            ColorButton((250, 200), (255, 0, 0), self.set_red),
            ColorButton((400, 200), (0, 255, 0), self.set_green),
            ColorButton((550, 200), (0, 0, 255), self.set_blue)
        ]

    def set_red(self):
        self.color = (255, 0, 0)

    def set_green(self):
        self.color = (0, 255, 0)

    def set_blue(self):
        self.color =  (0, 0, 255)

    def run_controller(self):
        pygame.init()
        screen = pygame.display.set_mode((800, 600))
        clock = pygame.time.Clock()

        while True:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    sys.exit()

                for button in self.buttons:
                    button.handle_event(event)

            self.color=self.buttons

            for button in self.buttons:
                button.draw(screen)

            self.color=self.buttons

            clock.tick(60)

